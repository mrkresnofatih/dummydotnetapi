﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace DummyApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DummyController : ControllerBase
    {
        public DummyController()
        {
            _appName = Environment.GetEnvironmentVariable("DUMMY_APP_NAME");
        }

        private readonly string _appName;
        
        [HttpGet("hello")]
        public object GetHello()
        {
            return new {Data = $"Hello again! - {_appName}"};
        }

        [HttpGet("hi")]
        public object GetHi()
        {
            return new {Data = $"Hi there!, I'm {_appName}."};
        }
    }
}