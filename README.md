﻿# DummyAPI

### Run Application

To run the app, simply use docker for this. Follow these steps:
1. Run `docker build .`
2. Run `docker run -d -p <port>:80 -e DUMMY_APP_NAME=FatihApp <imageID>`